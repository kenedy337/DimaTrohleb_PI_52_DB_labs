#1. Запит на знищення даних з таблиці «Reiting» за визначеним кодом студента (в поле параметра вводиться прізвище студента).
DELIMITER //
CREATE PROCEDURE proc1(IN Fname VARCHAR(50))
  BEGIN
    DELETE r FROM Reiting r INNER JOIN dbo_student s ON r.Kod_student = s.Kod_stud
    WHERE s.Fname = Fname;
  END//


#2. Запит на знищення даних з таблиці «Para» за визначеним кодом дисципліни (у поле параметра вводиться назва дисципліни).

#3. Запит на оновлення даних у таблиці «Reiting» – передбачити збільшення балів за грудень на 15%.
DELIMITER //
CREATE PROCEDURE proc3()
  BEGIN
    UPDATE Reiting
    SET Reiting.Reiting = (Reiting.Reiting * 1.15)
    WHERE Reiting.K_zapis IN (SELECT Rozklad_pids.K_zapis
                              FROM Rozklad_pids
                              WHERE month(Rozklad_pids.Date) = 12);
  END//

#4. Запит на оновлення даних в таблиці «Reiting»– передбачити зменшення балів за іспит на 15%.
DELIMITER //
CREATE PROCEDURE proc4()
  BEGIN
    UPDATE Reiting
    SET Reiting.Reiting = (Reiting.Reiting * 0.75)
    WHERE Reiting.K_zapis IN (SELECT Rozklad_pids.K_zapis
                              FROM Rozklad_pids
                                INNER JOIN Predmet_plan ON Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
                                INNER JOIN form_kontr ON form_kontr.k_fk = Predmet_plan.k_fk);
  END//

#5. Запит на вставку даних до таблиці «Reiting» – передбачити вставку даних студентів визначеної групи (код групи та початковий бал задається динамічно).
DELIMITER //
CREATE PROCEDURE proc5(kod_stud INT(11), bal INT(4))
  BEGIN
    INSERT INTO Reiting (Kod_student, Reiting)
    VALUES (kod_stud, bal);
  END//

#6. Запит на вставку даних до таблиці «Para» – передбачити вставку всіх дисциплін, назва яких починається з літери «М» (дата заняття та години задаються динамічно, однакові для всіх). 


#7. Запит на оновлення даних – передбачити зміну порядкового номера відомості за певною дисципліною на нове значення.
DELIMITER //
CREATE PROCEDURE proc7(predmet VARCHAR(50), vedomost INT(4))
  BEGIN
    UPDATE Rozklad_pids
    SET Rozklad_pids.N_vedomost = vedomost
    WHERE Rozklad_pids.K_predm_pl IN (
      SELECT Predmet_plan.K_predm_pl
      FROM Predmet_plan
        INNER JOIN predmet ON predmet.K_predmet = Predmet_plan.K_predmet
      WHERE predmet.Nazva = predmet);
  END//

#8. Передбачити знищення студентів з таблиці «Students» за визначеним номером групи.
DELIMITER //
CREATE PROCEDURE proc8(kod_group VARCHAR(50))
  BEGIN
    DELETE FROM dbo_student
    WHERE dbo_student.Kod_group = kod_group;
  END//

#9. Запит на вставку даних до таблиці «Reiting» – передбачити вставку даних студентів визначеної групи (код пари та присутність задаються динамічно)
DELIMITER //
CREATE PROCEDURE proc9(kod_group VARCHAR(50), prisutn INT(1), k_zapis INT(11))
  BEGIN
    INSERT INTO Reiting (Kod_student, Prisutn, K_zapis)
      SELECT
        dbo_student.Kod_stud,
        prisutn,
        k_zapis
      FROM dbo_student
        INNER JOIN dbo_groups ON dbo_groups.Kod_group = dbo_student.Kod_group;
  END//

#10. Передбачити оновлення даних у таблиці «Reiting»  поле «Prisutnist», для студента із визначеним прізвищем автоматично встановлюється true.
DELIMITER //
CREATE PROCEDURE proc10(student_Name VARCHAR(50))
  BEGIN
    UPDATE Reiting
    SET Reiting.Prisutn = 1
    WHERE Reiting.Kod_student IN (SELECT dbo_student.Kod_stud
                                  FROM dbo_student
                                  WHERE dbo_student.SName = student_Name);
  END//
