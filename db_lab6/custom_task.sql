/* TASK 1 */

set SQL_SET_UPDATES = 0;
use VikladachkiySklad;

INSERT IGNORE INTO Groups(group_name, course) values ("PI_52", 2);

UPDATE Teachers
SET birthday = '1990-10-10'
WHERE Teachers.full_name = "Trohlib Dmitro";

UPDATE Teachers
SET full_name = 'Trohlib Dmitro'
WHERE Teachers.full_name like "1990-10-10";

select * from Teachers;

/* TASK 2 */

set FOREIGN_KEY_CHECKS=0;
ALTER TABLE Subjects MODIFY COLUMN
  id INT(6) AUTO_INCREMENT;
set FOREIGN_KEY_CHECKS=1;

UPDATE Teachers
SET full_name = CONCAT(full_name, FROM_UNIXTIME(unix_timestamp()))
WHERE full_name REGEXP "Dmitro";

/* TASK 3 */
use VikladachkiySklad;
drop PROCEDURE if EXISTS task1;
DELIMITER //
CREATE PROCEDURE task1(in min int, in max int)
  BEGIN
select full_name as Tot_kto_delate_bolshe_srednego, (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) as hours from Navantazhennya
inner join Teachers on (Navantazhennya.id = id_Navantazhennya)
where (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) between min and max;
    END //

CALL task1(150, 180);


drop PROCEDURE if EXISTS task2;
DELIMITER //
CREATE PROCEDURE task2(in  first_group nvarchar(50), in second_group nvarchar(50))
  BEGIN
select distinct Teachers.full_name, Subjects.math, Subjects.programming,
Subjects.history, Subjects.informatica, Subjects.politology, Subjects.english from TeacherGroup
inner join Groups on (TeacherGroup.id_Group = Groups.id)
inner join Teachers on (TeacherGroup.id_Teacher = Teachers.id)
inner join Subjects on (Teachers.id = Subjects.id)
where group_name = first_group or group_name = "PIK_14"; 
   END //

CALL task2("PI_52", "PIK_14");

drop procedure if exists task3;
DELIMITER //
CREATE PROCEDURE task3(out averege nvarchar(50))
	BEGIN
set averege = (select group_name from Groups
where group_name like "PIK_14" and course = 2);
	END//
  DELIMITER ;  
where navantazhennya > 30;
END //
set @av = 0;
call task3(@av);
select @av;
