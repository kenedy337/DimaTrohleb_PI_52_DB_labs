/* selects for laba 4 */
use VikladachkiySklad;

/*1*/
select * from Teachers where date_sub(current_date(), interval 19 year) > birthday;

/*2*/
select full_name, (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) as general_navantazhennya from Teachers 
inner join Navantazhennya on (Navantazhennya.id = id_Navantazhennya)
where (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) > 160 and family = "married";

/*3*/
select Teachers.full_name, Groups.group_name from TeacherGroup
inner join Groups on (TeacherGroup.id_Group = Groups.id)
inner join Teachers on (TeacherGroup.id_Teacher = Teachers.id)
where Teachers.full_name = 'Lisovii Evgenii';

/*4*/
select Teachers.full_name, math, programming, history, informatica, politology, english from Subjects
inner join Teachers on (id_Subjects = Subjects.id)
where math = true;

/*5*/
select Teachers.full_name, Groups.group_name from TeacherGroup
inner join Groups on (TeacherGroup.id_Group = Groups.id)
inner join Teachers on (TeacherGroup.id_Teacher = Teachers.id)
inner join Subjects on (Teachers.id = Subjects.id)
where group_name = 'PI_52' and math = true;

/*6*/
select full_name from Teachers
inner join TeacherType on (TeacherType.id = Teachers.id_TeachRole)
inner join Navantazhennya on (Navantazhennya.id = Teachers.id_Navantazhennya)
where shtatni = true and lectures > 25;

/*7*/
select full_name, birthday from Teachers
where birthday = (select max(birthday) from Teachers) and nauk_stupin = true;

/*8*/
select full_name, job, (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) as general_navantazhennya from Teachers
inner join Navantazhennya on (Navantazhennya.id = id_Navantazhennya)
where job like '%web%' and (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) > 120;


/*9*/
select avg(lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) as Spedne  from Navantazhennya;

/*10*/
select Teachers.full_name from Navantazhennya
inner join Teachers on (id_Navantazhennya = Navantazhennya.id)
where lectures = 40 and practic_works between 10 and 30;











