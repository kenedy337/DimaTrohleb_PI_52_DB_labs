# 1)
SELECT
  tovar.Nazva_t,
  z.*
FROM tovar, zakaz z INNER JOIN zakaz_tovar zt ON (zt.id_zakaz = z.K_zakaz)
  INNER JOIN tovar t ON (zt.id_tovar = t.id_tovar)
WHERE z.Date_naznach < '2017-12-12' AND t.Nazva_t = 'Vazon';

# 2)
SELECT *
FROM tovar
WHERE Zap_skl >= 15 AND price > 15 AND price < 40;

# 3)
SELECT *
FROM zakaz
WHERE Date_naznach IS NULL;

# 4)
SELECT t.*
FROM tovar t INNER JOIN postachalnik p ON (p.id_postach = t.id_postach)
WHERE t.Zap_skl = 648 AND p.Nazva = "Babikue";

# 5)
SELECT SUM(t.price)
FROM zakaz z INNER JOIN zakaz_tovar zt ON (zt.id_zakaz = z.K_zakaz)
  INNER JOIN tovar t ON (zt.id_tovar = t.id_tovar)
WHERE z.Date_rozm > NOW() - INTERVAL 30 DAY;

# 6)
SELECT
  s.id_sotrudnik,
  z.Date_rozm
FROM Sotrudnik s INNER JOIN zakaz z ON (z.K_sotrud = s.id_sotrudnik)
  INNER JOIN klient k ON (z.K_klien = k.id_klient)
WHERE k.Nazva = 'Kolka';

# 7)
SELECT p.*, zt.id_zakaz
FROM postachalnik p
  INNER JOIN tovar t ON (t.id_postach = p.id_postach)
  LEFT JOIN zakaz_tovar zt ON (zt.id_tovar = t.id_tovar)
WHERE id_zakaz IS NULL;

# 8)
SELECT *
FROM klient k INNER JOIN zakaz z ON (z.K_klien = k.id_klient)
WHERE z.Date_naznach LIKE CONCAT(YEAR(CURRENT_DATE), '-',
                                 IF(MONTH(CURRENT_DATE) - 1 < 10, CONCAT('0', MONTH(CURRENT_DATE) - 1),
                                    MONTH(CURRENT_DATE) - 1), '%');

# 9)
SELECT *
FROM Sotrudnik
WHERE Name_S = 'Zemelua'
ORDER BY Fname ASC;

# 10)

ALTER TABLE klient
  ADD COLUMN email VARCHAR(50) DEFAULT NULL;
UPDATE klient
SET email = 'trohlib@gmail.com'
WHERE Nazva = 'trohlib';
UPDATE klient
SET email = 'dima@mail.ua'
WHERE Nazva = 'dima';

SELECT *
FROM klient
WHERE email IS NOT NULL
ORDER BY id_klient;
