DROP DATABASE IF EXISTS torg_firm;
CREATE DATABASE IF NOT EXISTS torg_firm;
USE torg_firm;

CREATE TABLE postachalnik (
  id_postach INT NOT NULL AUTO_INCREMENT,
  Nazva      VARCHAR(20),
  Index_p    CHAR(5)      DEFAULT '10000',
  Addr       VARCHAR(50),
  City       VARCHAR(20),
  Region     VARCHAR(50),
  tel        CHAR(12) CHECK (tel LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  PRIMARY KEY (id_postach),
  CONSTRAINT nazva_format CHECK (Nazva LIKE '[А-я]%'),
  CONSTRAINT ind_format CHECK (index_p LIKE '[0-9][0-9][0-9][0-9][0-9]'),
  CONSTRAINT city_format CHECK (City LIKE '[А-я]%')
);

INSERT INTO postachalnik (Nazva, Index_p, Addr, City, Region, tel)
VALUES ('Babikue', 10023, 'Hoverna 31', 'Zhytomyr', 'Zhytomyrska', 4685649),
  ('Galka', 10023, 'Glovery 82', 'Zhytomyr', 'Zhytomyrska', 4685739),
  ('Kolka', 10025, 'Bernan 92', 'Zhytomyr', 'Zhytomyrska', 4685649),
  ('Gimpa', 10026, 'Klovery 82', 'Zhytomyr', 'Zhytomyrska', 21423324),
  ('Dalart', 10027, 'Pernan 23', 'Zhytomyr', 'Zhytomyrska', 4685234),
  ('Lordans', 10027, 'Derna 23', 'Zhytomyr', 'Zhytomyrska', 4685234),
  ('Cooliti', 10028, 'Dnipro 1', 'Zhytomyr', 'Zhytomyrska', 654231341);

CREATE TABLE tovar (
  id_tovar   INT AUTO_INCREMENT NOT NULL,
  Nazva_t    VARCHAR(20),
  Min_zap    INT DEFAULT 10 CHECK (Min_zap > 0),
  Zap_skl    INT,
  price      DECIMAL,
  post_prek  SMALLINT CHECK (post_prek IN (0, 1)),
  id_postach INT                NOT NULL,
  PRIMARY KEY (id_tovar),
  FOREIGN KEY (id_postach) REFERENCES
    postachalnik (id_postach)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT nazva_tf CHECK (Nazva_t LIKE '[А-я]%')
);

INSERT INTO tovar (Nazva_t, Min_zap, Zap_skl, price, post_prek, id_postach)
VALUES
  ('Vazon', 11, 648, 45, 1, 1),
  ('Kirpich', 11, 848, 35, 1, 2),
  ('Horse', 11, 648, 45, 1, 4),
  ('Vertigo', 11, 848, 35, 1, 5),
  ('Vetrilo', 10, 441, 18, 0, 5),
  ('Gilioutina', 10, 421, 7, 1, 6),
  ('Pijama', 10, 441, 18, 0, 7)
;

CREATE TABLE klient (
  id_klient INT NOT NULL AUTO_INCREMENT,
  Nazva     VARCHAR(20),
  Index_p   CHAR(5)      DEFAULT '10000',
  Addr      VARCHAR(50),
  City      VARCHAR(20),
  Region    VARCHAR(50),
  tel       CHAR(12) CHECK (tel LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  PRIMARY KEY (id_klient),
  CONSTRAINT nazva_format CHECK (Nazva LIKE '[А-я]%'),
  CONSTRAINT ind_format CHECK (index_p LIKE '[0-9][0-9][0-9][0-9][0-9]'),
  CONSTRAINT city_format CHECK (City LIKE 'м. [А-я]%')
);

ALTER TABLE klient
  ADD COLUMN email VARCHAR(50) DEFAULT NULL;

INSERT INTO klient (Nazva, Index_p, Addr, City, Region, tel)
VALUES ('dima', 10034, 'Malikova 12', 'Ololosheno', 'Loleno', 148390),
  ('dima', 10023, 'Malikova 13', 'Ololosheno', 'Loleno', 73493),
  ('evgen', 10035, 'Malikova 14', 'Ololosheno', 'Loleno', 1483451),
  ('maxim', 10036, 'Malikova 15', 'Ololosheno', 'Loleno', 3214124),
  ('yarik', 10037, 'Malikova 16', 'Ololosheno', 'Loleno', 143124),
  ('ruslana', 10038, 'Malikova 17', 'Ololosheno', 'Loleno', 512442);


CREATE TABLE Sotrudnik (
  id_sotrudnik  INT AUTO_INCREMENT NOT NULL,
  Fname         VARCHAR(20),
  Name_S        VARCHAR(20),
  Posada        VARCHAR(40),
  Date_enter    DATE,
  Date_birthday DATE,
  Index_p       CHAR(5) DEFAULT '10000',
  Adress        VARCHAR(50),
  City          VARCHAR(20),
  Region        VARCHAR(20),
  Country       VARCHAR(20),
  Tel           CHAR(10) CHECK (tel LIKE '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  Foto          VARCHAR(50),
  Note          TEXT,
  PRIMARY KEY (id_sotrudnik),
  CONSTRAINT Fname_format CHECK (Fname LIKE '[А-Я]%'),
  CONSTRAINT name_format CHECK (Fname LIKE '[А-Я]%'),
  CONSTRAINT posada_format CHECK (posada LIKE '[А-Я]%'),
  CONSTRAINT ind_format CHECK (index_p LIKE '[0-9][0-9][0-9][0-9][0-9]'),
  CONSTRAINT city_format CHECK (city LIKE '[А-Я]%'),
  CONSTRAINT region_format CHECK (region LIKE '[А-Я]%'),
  CONSTRAINT country_format CHECK (country LIKE '[А-Я]%')
);

INSERT INTO Sotrudnik (Fname, Name_S, Posada, Date_enter, Date_birthday, Index_p, Adress, City, Region, Country, Tel, Foto, Note)
VALUES
  ('Klipov', 'Zemelua', 'engeenir', '2015.02.01', '1987.01.04', 10056, 'Borscheva 93', 'Zhytomyr', 'Zhytomyrska', 'Ukraine',
             76574, '/image.jpg', 'description'),
  ('Antipov', 'Kirill', 'engeenir', '2011.10.09', '1995.09.14', 10066, 'Glebova 23', 'Zhytomyr', 'Zhytomyrska', 'Ukraine',
            74593, '/image2.jpg', 'hobby'),
  ('Kobizev', 'Cloverka', 'engeenir', '2015.03.10', '1990.10.14', 10056, 'Antonovka 24', 'Zhytomyr', 'Zhytomyrska', 'Ukraine',
            76574, '/image3.jpg', 'description'),
  ('Dimasta', 'Aristotel', 'engeenir', '2013.12.15', '1996.05.04', 10066, 'Antonovka 13', 'Zhytomyr', 'Zhytomyrska',
               'Ukraine', 74593, '/image4.jpg', 'hobby'),
  ('Jeremy', 'Clarkson', 'engeenir', '2013.09.25', '2000.09.01', 10066, 'Kyivstar 1', 'Zhytomyr', 'Zhytomyrska', 'Ukraine',
              123213, '/image5.jpg', 'Great boxer'),
  ('Hostel', 'Gomer', 'secretar', '2013.10.09', '1991.01.10', 10066, 'Loeva 6', 'Zhytomyr', 'Zhytomyrska', 'Ukraine',
              123213, '/image6.jpg', 'Great boxer');

CREATE TABLE zakaz_tovar (
  id_zakaz INT NOT NULL REFERENCES zakaz (id_zakaz)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  id_tovar INT NOT NULL REFERENCES tovar (id_tovar)
    ON UPDATE NO ACTION
    ON DELETE CASCADE,
  kilk     INT CHECK (kilk > 0),
  znigka   DECIMAL(3, 2),
  PRIMARY KEY (id_zakaz, id_tovar)
);

INSERT INTO zakaz_tovar (id_zakaz, id_tovar, kilk, znigka)
VALUES
  (1, 1, 34, 0.2),
  (2, 2, 23, 0.1),
  (1, 10, 34, 0),
  (2, 3, 43, 0),
  (2, 4, 232, 0),
  (3, 5, 43, 0),
#   (4, 7, 612, 0.2),
  (5, 8, 66, 0),
  (6, 9, 92, 0.1),
  (6, 1, 1, 0),
  (6, 4, 43, 0),
  (7, 6, 62, 0),
  (7, 1, 45, 0),
  (8, 6, 82, 0),
  (8, 4, 324, 0.1),
  (8, 1, 43, 0),
  (8, 3, 24, 0.1),
  (9, 8, 325, 0.4),
  (9, 3, 834, 0),
  (10, 9, 93, 0),
  (10, 5, 443, 0);

CREATE TABLE zakaz (
  K_zakaz      INT AUTO_INCREMENT PRIMARY KEY,
  K_klien      INT REFERENCES klient (id_klient),
  K_sotrud     INT REFERENCES sotrudnik (id_sotrudnik),
  Date_rozm    DATE,
  Date_naznach DATE
);

INSERT INTO zakaz (K_klien, K_sotrud, Date_rozm, Date_naznach)
VALUES
  (1, 1, '2017.07.04', '2017.07.08'),
  (2, 2, '2017.07.09', '2017.07.10'),
  (1, 1, '2017.07.04', '2017.07.08'),
  (2, 2, '2017.03.06', '2017.07.08'),
  (3, 3, '2017.11.04', '2017.12.25'),
  (4, 4, '2017.09.10', '2017.10.08'),
  (5, 6, '2017.05.19', '2017.04.03'),
  (6, 5, '2017.02.23', '2017.03.12'),
  (4, 2, '2017.12.01', '2017.12.16'),
  (4, 2, '2017.12.01', NULL),
  (2, 6, '2017.08.17', '2017.09.23');



