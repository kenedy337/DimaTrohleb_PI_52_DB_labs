create database VikladachkiySklad;
use VikladachkiySklad;

drop table Teachers;

create table Teachers(
id int(5) primary key auto_increment,
id_Navantazhennya int(5) not null,
id_TeachRole int(5) not null,
id_Subjects int(5) not null,
full_name nvarchar(100) not null,
address nvarchar(100) not null,
phone nvarchar(50) not null,
job nvarchar(50) not null,
family nvarchar(50) not null,
birthday date,
nauk_stupin bool not null,
vchenne_zvannya bool not null,
foreign key (id_Navantazhennya) references Navantazhennya(id) on delete cascade on update cascade,
foreign key (id_TeachRole) references TeacherType(id) on delete cascade on update cascade,
foreign key (id_Subjects) references Subjects(id) on delete cascade on update cascade);

create table Subjects(
id int(5) key auto_increment,
math bool,
programming bool,
history bool,
informatica bool,
politology bool,
english bool);

create table Navantazhennya(
id int(5) primary key auto_increment,
lectures int(5) not null,
practic_works int(5) not null,
lab_works int(5) not null,
modul_control int(5) not null,
exams int(5) not null,
credits int(5) not null,
consultations int(5) not null,
diploma_works int(5) not null,
course_works int(5) not null);

create table TeacherGroup(
id int(5) primary key auto_increment,
id_Teacher int(5) not null,
id_Group int(5) not null);

create table Groups(
id int(5) primary key auto_increment,
group_name nvarchar(20) not null,
course int(1) not null);

create table TeacherType(
id int(5) primary key auto_increment,
shtatni bool not null,
zaprosheni bool not null);

/* SELECTS */
SELECT * FROM Teachers 
inner join TeacherType on (Teachers.id_TeachRole = TeacherType.id)
where shtatni = true;

SELECT * FROM Teachers 
inner join TeacherType on (Teachers.id_TeachRole = TeacherType.id)
where zaprosheni = true;

select * from Teachers where nauk_stupin = true;

select sum(lectures), sum(practic_works), sum(lab_works), sum(modul_control), sum(exams), 
sum(credits), sum(consultations), sum(diploma_works), sum(course_works) from Navantazhennya;

select Teachers.full_name, lectures, practic_works, lab_works, modul_control, 
exams, credits, consultations, diploma_works, course_works from Navantazhennya
inner join Teachers on (Teachers.id_Navantazhennya = Navantazhennya.id)
where full_name = "Maksim Novik";

/* SELECT ALL */
select * from Teachers;
select * from Navantazhennya;
select * from Groups;
select * from TeacherType;
select * from TeacherGroup;
select * from Subjects;

/* INSERTS */
insert into Teachers(id_Navantazhennya, id_TeachRole, id_Subjects, full_name, address, phone, job, family, birthday, nauk_stupin, vchenne_zvannya) values
(1, 1, 1, "Trohlib Dmitro", "Malikova 12", "+123456789", "web developer", "married", '1996-12-20', true, true);
insert into Teachers(id_Navantazhennya, id_TeachRole, id_Subjects, full_name, address, phone, job, family, birthday, nauk_stupin, vchenne_zvannya) values
(2, 2, 2, "Maksim Novik", "Malikova 15", "+987654321", "php web developer", "single", '1998-06-26', true, true);
insert into Teachers(id_Navantazhennya, id_TeachRole, id_Subjects, full_name, address, phone, job, family, birthday, nauk_stupin, vchenne_zvannya) values
(3, 3, 3, "Lisovii Evgenii", "Malikova 18", "+111222333", "mysql developer", "married", '1997-10-10', false, false);

insert into Navantazhennya(lectures, practic_works, lab_works, modul_control, exams, credits, consultations, diploma_works, course_works) values
(30, 60, 12, 6, 4, 4, 10, 30, 30);
insert into Navantazhennya(lectures, practic_works, lab_works, modul_control, exams, credits, consultations, diploma_works, course_works) values
(20, 40, 20, 12, 8, 8, 15, 20, 40);
insert into Navantazhennya(lectures, practic_works, lab_works, modul_control, exams, credits, consultations, diploma_works, course_works) values
(40, 20, 10, 5, 10, 10, 5, 20, 40);

insert into Subjects(math, programming, history, informatica, politology, english) values
(true, true, false, false, false, false);
insert into Subjects(math, programming, history, informatica, politology, english) values
(true, false, true, false, true, false);
insert into Subjects(math, programming, history, informatica, politology, english) values
(false, true, false, true, false, true);

insert into Groups(group_name, course) values ("PI_52", 2);
insert into Groups(group_name, course) values ("PI_55", 1);
insert into Groups(group_name, course) values ("PI_51", 2);
insert into Groups(group_name, course) values ("PIK_14", 2);
insert into Groups(group_name, course) values ("PIK_15", 1);
insert into Groups(group_name, course) values ("ATF_23", 2);
insert into Groups(group_name, course) values ("ATF_12", 3);
insert into Groups(group_name, course) values ("PI_46", 3);
insert into Groups(group_name, course) values ("OTK_32", 2);

insert into TeacherGroup(id_Teacher, id_Group) values (1, 1);
insert into TeacherGroup(id_Teacher, id_Group) values (1, 2);
insert into TeacherGroup(id_Teacher, id_Group) values (1, 3);
insert into TeacherGroup(id_Teacher, id_Group) values (2, 4);
insert into TeacherGroup(id_Teacher, id_Group) values (2, 5);
insert into TeacherGroup(id_Teacher, id_Group) values (2, 6);
insert into TeacherGroup(id_Teacher, id_Group) values (3, 7);
insert into TeacherGroup(id_Teacher, id_Group) values (3, 8);
insert into TeacherGroup(id_Teacher, id_Group) values (3, 9);