CREATE DATABASE Hospital;
use Hospital;

drop table Ambulator_Card;
drop table Complaints;
drop table Lab_Date;
drop table Re_appointment;
drop table Consultations;
drop table Doctors;
drop table Specialnost;
drop table Hospitals;

show tables;

create table Ambulator_Card(
id int(5) primary key auto_increment,
id_Complaints int(5) not null,
id_Lab_Date int(5) not null,
id_Re_appointment int(5) not null,
id_Consultations int(5) not null,
id_Hospitals int(5) not null,
full_name nvarchar(50) not null,
data_card date,
age int(3) not null,
job nvarchar(50) not null,
address nvarchar(50) not null,
phone nvarchar(50) not null,
id_hospital int(5) not null,
foreign key (id_Complaints) references Complaints(id) on delete cascade on update cascade,
foreign key (id_Lab_Date) references Lab_Date(id) on delete cascade on update cascade,
foreign key (id_Re_appointment) references Re_appointment(id) on delete cascade on update cascade,
foreign key (id_Consultations) references Consultations(id) on delete cascade on update cascade,
foreign key (id_Hospitals) references Hospitals(id)
);

create table Complaints(
id int(5) primary key auto_increment,
AnamesisMorbi nvarchar(100) NOT NULL,
AnamesisVitae nvarchar(100) NOT NULL
);

create table Lab_Date(
id int(5) primary key auto_increment,
gen_condition nvarchar(100) not null,
skin nvarchar(100) not null,
lymph nvarchar(100) not null
);

create table Re_appointment(
id int(5) primary key auto_increment,
complain NVARCHAR(100) not null,
gen_cond NVARCHAR(100) not null,
struct_syst nvarchar(100) not null,
cranial_syst nvarchar(100) not null,
number_appoint int(5) not null
);

create table Consultations(
id int(5) primary key auto_increment,
nevrolog nvarchar(50) not null,
pediatr nvarchar(50) not null,
ortoped nvarchar(50) not null,
travmatolog nvarchar(50) not null,
napravil nvarchar(50) not null
);

create table Doctors(
id int(5) primary key auto_increment,
id_specialnost int(5) not null,
id_hospital int(5) not null,
address nvarchar(50) not null,
phone nvarchar(50) not null,
foreign key (id_specialnost) references Specialnost(id),
foreign key (id_hospital) references Hospitals(id)
);

create table Specialnost(
id int(5) primary key auto_increment,
proffesions nvarchar(50) not null
);

create table Hospitals(
id int(5) primary key auto_increment,
hospital_name nvarchar(50) not null,
address nvarchar(50) not null,
phone nvarchar(50) not null
);


























