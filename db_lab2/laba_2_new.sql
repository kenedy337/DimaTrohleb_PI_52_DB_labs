create database bolnica;
use bolnica;

create table ambulator_card(
id int(5) primary key auto_increment,
full_name nvarchar(50) not null,
date_pacient nvarchar(50) not null,
age int(3) not null,
job nvarchar (50) not null,
address nvarchar(50) not null,
phone nvarchar(50) not null
);

create table specialnost(
id int(5) primary key auto_increment,
proffession nvarchar(50) not null
);

create table complaints(
id int(5) primary key auto_increment,
id_pacient int(5) not null references ambulator_card(id) on delete cascade on update cascade,
complain nvarchar(100) not null
);

create table diagnoz(
id int(5) primary key auto_increment,
id_pacient int(5) not null references ambulator_card(id) on delete cascade on update cascade,
diagnoz nvarchar(100) not null,
diagnoz_date date
);

create table consultacii(
id int(5) primary key auto_increment,
id_pacient int(5) not null references ambulator_card(id) on delete cascade on update cascade,
id_specialnost int(5) not null references specialnost(id) on delete cascade on update cascade,
id_napravil int(5) not null references vrachi(id) on delete cascade on update cascade,
date_consultacii nvarchar(50) not null
);

create table vrachi(
id int(5) primary key auto_increment,
id_specialnost int(5) references specialnost(id) on delete cascade on update cascade,
full_name nvarchar(50) not null,
age int(3) not null,
address nvarchar(50) not null,
phone nvarchar(50) not null);









