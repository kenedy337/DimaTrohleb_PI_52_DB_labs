#1. Розрахунок середнього балу студентів за період – без урахування перездач.
use `lr_5_reir_dekanat`

delimiter //

CREATE PROCEDURE avgWithoutP ()
BEGIN
  select AVG(r.Reiting) from Reiting r    
  INNER JOIN Rozklad_pids rp ON (r.K_zapis=rp.K_zapis) 
  WHERE Date BETWEEN '2003-10-08' AND '2017-12-12'
  GROUP BY Kod_student 
  HAVING COUNT(*)<2;
end//

delimiter ;
call avgWithoutP();
#2. Розрахунок середнього балу студентів за період – з урахування перездач.
delimiter //
CREATE PROCEDURE avgWithP ()
BEGIN
  select AVG(r.Reiting) from Reiting r 
  INNER JOIN Rozklad_pids rp ON (r.K_zapis=rp.K_zapis) 
  WHERE Date BETWEEN '2003-10-08' AND '2017-12-12'
  GROUP BY Kod_student;
END//
delimiter ;
call avgWithP();
#3. Визначення студентів, що навчаються на 4 та 5.
delimiter //
CREATE PROCEDURE vidminnyki ()
BEGIN
  select s.Sname,s.Name,s.Fname,r.Reiting from dbo_student s 
  INNER JOIN Reiting r ON r.Kod_student=s.Kod_stud 
  WHERE Reiting>=74;
END//
delimiter ;
call vidminnyki;
#4. Процедура виводить суму балів та її значення в національній системі та ECTS.
delimiter //
CREATE PROCEDURE bals ()
BEGIN
  select s.Sname,s.Name,s.Fname, r.Reiting,
    IF(r.Reiting >= 90, 'Відмінно', IF(r.Reiting >=74,'Добре', IF(r.Reiting >=60,'Задовільно','Незадовільно'))) as 'Національна шкала',
    IF(r.Reiting >= 90, 'A', IF(r.Reiting >=82,'B', IF(r.Reiting >=74,'C',IF(r.Reiting >=67,'D',IF(r.Reiting >=60,'E',IF(r.Reiting >=37,'FX','F')))))) as 'ECTS'
    from dbo_student s 
  INNER JOIN Reiting r ON r.Kod_student=s.Kod_stud;
END//
delimiter ;
call bals();
#5. Тригер на вставку даних в таблицю студент – якщо код групи новий в таблицю додається група.
delimiter //

Create trigger checkStudents AFTER INSERT ON dbo_student FOR EACH ROW
BEGIN
 if((Select count(*) from dbo_groups WHERE Kod_group=NEW.Kod_group) = 0)
  then 
   INSERT INTO dbo_groups (Kod_group,Kod_men,Kod_zhurn,K_navch_plan,kilk) VALUES (NEW.Kod_group,NEW.Kod_group, NEW.Kod_group,NEW.Kod_group,1);
 END IF;
END//

delimiter ;

#6. Тригер на модифікацію даних з таблиці  студенти якщо більше немає студентів в групі група знищується. 
delimiter //
Create trigger checkGroup AFTER UPDATE ON dbo_student FOR EACH ROW
BEGIN
 if((Select count(*) from dbo_student WHERE Kod_group=OLD.Kod_group) = 0)
  then 
   DELETE FROM dbo_groups WHERE Kod_group=OLD.Kod_group;
 END IF;
END//

delimiter ;
