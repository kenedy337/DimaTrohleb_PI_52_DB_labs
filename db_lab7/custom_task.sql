use `VikladachkiySklad`;

drop procedure tasker1;

DROP PROCEDURE IF EXISTS tasker1;
DELIMITER //
CREATE PROCEDURE tasker1(in parametr int)
  BEGIN
    SELECT
Teachers.full_name, TeacherType.shtatni, TeacherType.zaprosheni from Teachers
inner join TeacherType on (TeacherType.id = parametr)
order by full_name desc;
  END;
//
CALL tasker1(1);


DROP PROCEDURE IF EXISTS tasker2;
DELIMITER //
CREATE PROCEDURE tasker2(in group_search nvarchar(50))
  BEGIN
select distinct Teachers.full_name, Groups.group_name from TeacherGroup
inner join Groups on (TeacherGroup.id_Group = Groups.id)
inner join Teachers on (TeacherGroup.id_Teacher = Teachers.id)
inner join Subjects on (Teachers.id = Subjects.id)
where group_name = group_search and math = true;
  END;
//

CALL tasker2("PI_52");

DROP PROCEDURE IF EXISTS tasker3;
DELIMITER //
CREATE PROCEDURE tasker3()
  BEGIN
select full_name, job, (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) as general_navantazhennya from Teachers
inner join Navantazhennya on (Navantazhennya.id = id_Navantazhennya)
where job like '%web%' and (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) > 120;
  END//
CALL tasker3();

#-----------------------------------------------------------------------------------------------------------------------

DROP TRIGGER IF EXISTS  Cheker12;

DELIMITER //
CREATE TRIGGER Cheker12
before INSERT ON `Teachers`
FOR EACH ROW
  BEGIN
    IF ((SELECT count(*)
         FROM Teachers
         WHERE Teachers.full_name = NEW.full_name) > 0)
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'This name is already exist!';
    END IF;
  END//
  
insert into Teachers(id_Navantazhennya, id_TeachRole, id_Subjects, full_name, address, phone, job, family, birthday, nauk_stupin, vchenne_zvannya) values
(2, 1, 3, "Kaimon Novik", "Malikova 15", "+987654321", "php web developer", "single", '1998-06-26', true, true);

DROP TRIGGER IF EXISTS  Cheker13;
DELIMITER //
CREATE TRIGGER Cheker13
AFTER INSERT ON Teachers
FOR EACH ROW
  BEGIN
    IF ((SELECT count(*)
         FROM Teachers
         WHERE job = NEW.job) > 0)
    THEN
       SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT ='Эти учителя имеют одинаковую работу!';
    END IF;
  END//

DROP TRIGGER IF EXISTS cheker14;
DELIMITER //
CREATE TRIGGER cheker14
before INSERT ON Teachers
FOR EACH ROW
    BEGIN
    IF ((SELECT count(*)
         FROM Teachers
         WHERE date_sub(current_date(), interval 18 year) < new.birthday) > 0)
    THEN
       SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT ='This teacher is too youngfor our school, maybe we should to buy him a lolipop?';
    END IF;
  END//

insert into Teachers(id_Navantazhennya, id_TeachRole, id_Subjects, full_name, address, phone, job, family, birthday, nauk_stupin, vchenne_zvannya) values
(2, 1, 3, "Kaimon Novik", "Malikova 15", "+987654321", "php web developer", "single", '2001-06-26', true, true);

