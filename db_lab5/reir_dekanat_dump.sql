-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: reir_dekanat
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dbo_groups`
--

DROP TABLE IF EXISTS `dbo_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbo_groups` (
  `Kod_group` varchar(7) NOT NULL,
  `Kod_men` int(11) DEFAULT NULL,
  `Kod_zhurn` int(11) DEFAULT NULL,
  `K_navch_plan` int(11) NOT NULL,
  `kilk` int(11) DEFAULT NULL,
  PRIMARY KEY (`Kod_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_groups`
--

LOCK TABLES `dbo_groups` WRITE;
/*!40000 ALTER TABLE `dbo_groups` DISABLE KEYS */;
INSERT INTO `dbo_groups` VALUES ('1',1,1,1,23),('2',2,2,2,33),('3',3,3,3,26);
/*!40000 ALTER TABLE `dbo_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo_student`
--

DROP TABLE IF EXISTS `dbo_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbo_student` (
  `Name_ini` varchar(100) DEFAULT NULL,
  `Kod_stud` int(11) NOT NULL AUTO_INCREMENT,
  `Sname` varchar(25) DEFAULT NULL,
  `Name` varchar(25) DEFAULT NULL,
  `Fname` varchar(25) DEFAULT NULL,
  `N_ingroup` int(11) DEFAULT NULL,
  `Kod_group` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`Kod_stud`),
  KEY `FK_dbo_student_dbo_groups` (`Kod_group`),
  CONSTRAINT `FK_dbo_student_dbo_groups` FOREIGN KEY (`Kod_group`) REFERENCES `dbo_groups` (`Kod_group`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo_student`
--

LOCK TABLES `dbo_student` WRITE;
/*!40000 ALTER TABLE `dbo_student` DISABLE KEYS */;
INSERT INTO `dbo_student` VALUES ('ПОН',1,'Петров','Остап','Николаевич',1,'1'),('КИФ',2,'Кравец','Ирина','Федоровна',2,'1'),('ВВЛ',3,'Василевская','Виктория','Деонидовна',3,'1'),('ВАИ',4,'Власюк','Артем','Иванович',7,'2'),('ГСТ',5,'Гурский','Степан','Тимофеевич',9,'2'),('КПО',6,'Кучинский ','Павел','Олегович',11,'3');
/*!40000 ALTER TABLE `dbo_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_kontr`
--

DROP TABLE IF EXISTS `form_kontr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_kontr` (
  `k_fk` int(11) NOT NULL AUTO_INCREMENT,
  `forma_kontr` varchar(50) DEFAULT NULL,
  `Prim` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`k_fk`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_kontr`
--

LOCK TABLES `form_kontr` WRITE;
/*!40000 ALTER TABLE `form_kontr` DISABLE KEYS */;
INSERT INTO `form_kontr` VALUES (1,'Экзамен',',,,'),(2,'Зачет','...'),(3,'Опрос','...');
/*!40000 ALTER TABLE `form_kontr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_navch`
--

DROP TABLE IF EXISTS `form_navch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_navch` (
  `K_form` varchar(5) NOT NULL,
  `V_form` varchar(20) DEFAULT NULL,
  `prim` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`K_form`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_navch`
--

LOCK TABLES `form_navch` WRITE;
/*!40000 ALTER TABLE `form_navch` DISABLE KEYS */;
INSERT INTO `form_navch` VALUES ('1','дневная','...'),('2','вечерняя','...'),('3','заочная','...'),('4','индивидуальная','...');
/*!40000 ALTER TABLE `form_navch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Navch_plan`
--

DROP TABLE IF EXISTS `Navch_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Navch_plan` (
  `K_Navch_plan` int(11) NOT NULL AUTO_INCREMENT,
  `K_spets` int(11) NOT NULL,
  `year_nabor` datetime DEFAULT NULL,
  `year_vypusk` datetime DEFAULT NULL,
  `k_form` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`K_Navch_plan`),
  KEY `FK_Navch_plan_Form_navch1` (`k_form`),
  KEY `R_18` (`K_spets`),
  /*CONSTRAINT `FK_Navch_plan_Form_navch1`*/ FOREIGN KEY (`k_form`) REFERENCES `form_navch` (`K_form`) ON DELETE CASCADE ON UPDATE CASCADE,
  /*CONSTRAINT `R_18`*/ FOREIGN KEY (`K_spets`) REFERENCES `spetsialnost` (`K_spets`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Navch_plan`
--

LOCK TABLES `Navch_plan` WRITE;
/*!40000 ALTER TABLE `Navch_plan` DISABLE KEYS */;
INSERT INTO `Navch_plan` VALUES (1,1,'2016-09-01 00:00:00','2020-06-01 00:00:00','1'),(2,2,'2015-09-01 00:00:00','2019-06-01 00:00:00','3'),(3,4,'2015-09-01 00:00:00','2019-06-01 00:00:00','2'),(4,3,'2016-09-01 00:00:00','2020-06-01 00:00:00','2');
/*!40000 ALTER TABLE `Navch_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Predmet`
--

DROP TABLE IF EXISTS `Predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Predmet` (
  `K_predmet` int(11) NOT NULL AUTO_INCREMENT,
  `Nazva` varchar(55) DEFAULT NULL,
  `Nazva_skor` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`K_predmet`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Predmet`
--

LOCK TABLES `Predmet` WRITE;
/*!40000 ALTER TABLE `Predmet` DISABLE KEYS */;
INSERT INTO `Predmet` VALUES (1,'Экономика','таптм'),(2,'Менеджмент','плж'),(3,'Программирование','млмжпм'),(4,'Матанализ','мьмжм'),(5,'Дискретная математика','тттб');
/*!40000 ALTER TABLE `Predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet_plan`
--

DROP TABLE IF EXISTS `Predmet_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Predmet_plan` (
  `K_predm_pl` int(11) NOT NULL AUTO_INCREMENT,
  `K_predmet` int(11) NOT NULL,
  `K_navch_plan` int(11) NOT NULL,
  `Chas_Lek` smallint(6) DEFAULT NULL,
  `Cahs_pr` smallint(6) DEFAULT NULL,
  `Chas_all` smallint(6) DEFAULT NULL,
  `Chas_Labor` smallint(6) DEFAULT NULL,
  `Chas_sem` smallint(6) DEFAULT NULL,
  `Kilk_modul` smallint(6) DEFAULT NULL,
  `Cahs_sam` smallint(6) DEFAULT NULL,
  `Semestr` smallint(6) DEFAULT NULL,
  `Shifr_predmet` varchar(20) DEFAULT NULL,
  `k_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`K_predm_pl`),
  KEY `FK_Predmet_plan_Form_kontr1` (`k_fk`),
  KEY `FK_Predmet_plan_predmet` (`K_predmet`),
  KEY `R_12` (`K_navch_plan`),
  /*CONSTRAINT `FK_Predmet_plan_Form_kontr1` */FOREIGN KEY (`k_fk`) REFERENCES `form_kontr` (`k_fk`),
  /*CONSTRAINT `FK_Predmet_plan_predmet` */FOREIGN KEY (`K_predmet`) REFERENCES `Predmet` (`K_predmet`),
  /* CONSTRAINT `R_12` */ FOREIGN KEY (`K_navch_plan`) REFERENCES `Navch_plan` (`K_navch_plan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Predmet_plan`
--

LOCK TABLES `Predmet_plan` WRITE;
/*!40000 ALTER TABLE `Predmet_plan` DISABLE KEYS */;
INSERT INTO `Predmet_plan` VALUES (1,1,1,16,32,48,0,6,2,2,1,'ненене',1),(2,2,3,8,16,24,2,3,1,2,1,'роророр',2),(3,4,4,12,12,24,4,2,3,1,2,'титити',3),(4,3,2,14,6,20,2,2,2,2,1,'мимими',2);
/*!40000 ALTER TABLE `Predmet_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reiting`
--

DROP TABLE IF EXISTS `Reiting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reiting` (
  `K_zapis` int(11) NOT NULL,
  `Kod_student` int(11) NOT NULL,
  `Reiting` smallint(6) DEFAULT NULL,
  `Prisutn` int(11) DEFAULT NULL,
  PRIMARY KEY (`K_zapis`,`Kod_student`),
   KEY `FK_Reiting_dbo_student` (`Kod_student`),
   /*CONSTRAINT `FK_Reiting_Rozklad_pids`*/ FOREIGN KEY (`K_zapis`) REFERENCES `rozklad_pids` (`K_zapis`),
   /*CONSTRAINT `FK_Reiting_dbo_student` */FOREIGN KEY (`Kod_student`) REFERENCES `dbo_student` (`Kod_stud`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reiting`
--

LOCK TABLES `Reiting` WRITE;
/*!40000 ALTER TABLE `Reiting` DISABLE KEYS */;
INSERT INTO `Reiting` VALUES (1,1,100,1),(2,2,45,0),(3,3,57,0),(4,4,79,1);
/*!40000 ALTER TABLE `Reiting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rozklad_pids`
--

DROP TABLE IF EXISTS `Rozklad_pids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rozklad_pids` (
  `K_zapis` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `K_predm_pl` int(11) NOT NULL,
  `Kod_group` varchar(7) NOT NULL,
  `k_vilkad` int(11) NOT NULL,
  `N_vedomost` int(11) DEFAULT NULL,
  `F_zdacha` int(11) NOT NULL,
  `Zdacha_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`K_zapis`),
  KEY `FK_Rozklad_pids_dbo_groups` (`Kod_group`),
  KEY `R_15` (`K_predm_pl`),
  KEY `R_17` (`k_vilkad`),
  /*CONSTRAINT `FK_Rozklad_pids_dbo_groups`*/ FOREIGN KEY (`Kod_group`) REFERENCES `dbo_groups` (`Kod_group`),
  /*CONSTRAINT `R_15`*/ FOREIGN KEY (`K_predm_pl`) REFERENCES `Predmet_plan` (`K_predm_pl`),
  /*CONSTRAINT `R_17`*/ FOREIGN KEY (`k_vilkad`) REFERENCES `vikladach` (`k_vilkad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rozklad_pids`
--

LOCK TABLES `Rozklad_pids` WRITE;
/*!40000 ALTER TABLE `Rozklad_pids` DISABLE KEYS */;
INSERT INTO `Rozklad_pids` VALUES (1,'2017-05-01 00:00:00',1,'1',1,1,1,1),(2,'2017-05-01 00:00:00',2,'2',1,1,1,1),(3,'2017-06-01 00:00:00',3,'3',1,1,1,1),(4,'2017-06-01 00:00:00',4,'3',1,1,1,1);
/*!40000 ALTER TABLE `Rozklad_pids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spetsialnost`
--

DROP TABLE IF EXISTS `Spetsialnost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Spetsialnost` (
  `Shifr` varchar(10) DEFAULT NULL,
  `Nazva` varchar(60) DEFAULT NULL,
  `Kvalif` varchar(50) DEFAULT NULL,
  `K_spets` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`K_spets`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Spetsialnost`
--

LOCK TABLES `Spetsialnost` WRITE;
/*!40000 ALTER TABLE `Spetsialnost` DISABLE KEYS */;
INSERT INTO `Spetsialnost` VALUES ('ПИ','Программист','Программист',1),('МЕ','Менеджер','Менеджер',2),('ЭКО','Экономист','Экономист',3),('ЭК','Эколог','Эколог',4);
/*!40000 ALTER TABLE `Spetsialnost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vikladach`
--

DROP TABLE IF EXISTS `Vikladach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vikladach` (
  `k_vilkad` int(11) NOT NULL AUTO_INCREMENT,
  `FIO` varchar(100) DEFAULT NULL,
  `SName` varchar(20) DEFAULT NULL,
  `Name` varchar(20) DEFAULT NULL,
  `FName` varchar(20) DEFAULT NULL,
  `Posada` varchar(20) DEFAULT NULL,
  `Zvannya` varchar(20) DEFAULT NULL,
  `Stupen` varchar(20) DEFAULT NULL,
  `Kafedra` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`k_vilkad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vikladach`
--

LOCK TABLES `Vikladach` WRITE;
/*!40000 ALTER TABLE `Vikladach` DISABLE KEYS */;
INSERT INTO `Vikladach` VALUES (1,'ПАН','Панишев','Андрей','Николаевич','Зав.кафедрой','Професор','Вищий','ПЗС'),(2,'КАВ','Кондратовец','Анна','Василиевна','Преподователь','Ассистент','Первый','ПЗС'),(3,'ПАИ','Петруков','Анатолий','Иванович','Преподователь','Доцент','Первый','ВТ'),(4,'СТП','Семенова','Татьяна','Павлова','Преподователь','Доцент','Первый','ЕМ');
/*!40000 ALTER TABLE `Vikladach` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-24 23:31:35
