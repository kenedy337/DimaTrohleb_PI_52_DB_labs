/* 1 */
select full_name as Tot_kto_delate_bolshe_srednego, (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) as hours from Navantazhennya
inner join Teachers on (Navantazhennya.id = id_Navantazhennya)
where (lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works) between 150 and 180;

/* 2 */
select distinct Teachers.full_name, Subjects.math, Subjects.programming,
Subjects.history, Subjects.informatica, Subjects.politology, Subjects.english from TeacherGroup
inner join Groups on (TeacherGroup.id_Group = Groups.id)
inner join Teachers on (TeacherGroup.id_Teacher = Teachers.id)
inner join Subjects on (Teachers.id = Subjects.id)
where group_name = "PI_52" or group_name = "PIK_14"; 

/* 3 */
select sum(lectures), sum(practic_works), sum(lab_works) from Navantazhennya
group by lectures
order by lectures desc;

/* 4 */
select full_name, ((lectures + practic_works + lab_works + modul_control + exams + 
credits + consultations + diploma_works + course_works)/9) as Spednyaya_nagruzka from Navantazhennya 
inner join Teachers on (Navantazhennya.id = id_Navantazhennya);

/* 5 */
select max(lectures), max(practic_works), max(lab_works), max(modul_control), max(exams), 
max(credits), max(consultations), max(diploma_works), max(course_works) as Spednyaya_nagruzka from Navantazhennya;

/* 6 */
select group_name, course from Groups
where group_name like "%PI%" and course = 2
order by group_name desc;
/* 7 */
select * from Navantazhennya
inner join Teachers on (Navantazhennya.id = Teachers.id_Navantazhennya)
where full_name = 'Maksim Novik' and family = 'single';
/* 8 */
select full_name, practic_works from Teachers
inner join Navantazhennya on (Navantazhennya.id = id_Navantazhennya)
inner join TeacherType on (TeacherType.id = id_TeachRole)
where practic_works > 20 and shtatni = true
order by full_name desc;

/* 9 */
select * from Groups
where course > 1
order by group_name
LIMIT 2,5;

/* 10 */
select sum(lectures), sum(practic_works), sum(lab_works), sum(modul_control), sum(exams), 
sum(credits), sum(consultations), sum(diploma_works), sum(course_works) from Navantazhennya
order by lectures 


