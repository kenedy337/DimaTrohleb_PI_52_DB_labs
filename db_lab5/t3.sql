SELECT
  dbo_student.Name,
  Predmet.Nazva,
  SUM(Reiting.Reiting) AS Reiting
FROM
  (((Reiting
    INNER JOIN dbo_student ON Reiting.Kod_student = dbo_student.Kod_stud)
    INNER JOIN Rozklad_pids ON Rozklad_pids.K_zapis = Reiting.K_zapis)
    INNER JOIN Predmet_plan ON Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl)
  INNER JOIN Predmet ON Predmet.K_predmet = Predmet_plan.K_predmet
GROUP BY dbo_student.Name, Predmet.Nazva;

SELECT
  dbo_groups.Kod_group,
  COUNT(dbo_student.Name) AS CountStuden
FROM dbo_groups
  INNER JOIN dbo_student ON dbo_student.Kod_group = dbo_groups.Kod_group
GROUP BY dbo_groups.Kod_group;

SELECT
  q.Kod_group,
  COUNT(q.Nazva) AS CountSubjects
FROM (SELECT DISTINCT
        dbo_groups.Kod_group,
        Predmet.Nazva
      FROM (dbo_groups
        INNER JOIN Rozklad_pids ON Rozklad_pids.Kod_group = dbo_groups.Kod_group)
        INNER JOIN Predmet_plan ON Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
        INNER JOIN Predmet ON Predmet.K_predmet = Predmet_plan.K_predmet) AS q
GROUP BY q.Kod_group;

SELECT
  Rozklad_pids.Kod_group,
  Count(Rozklad_pids.K_zapis) AS CountLessons
FROM Rozklad_pids
GROUP BY Rozklad_pids.Kod_group;

SELECT
  dbo_groups.Kod_group,
  AVG(Reiting.Reiting) AS AvgReit
FROM (dbo_groups
  INNER JOIN dbo_student ON dbo_student.Kod_group = dbo_groups.Kod_group)
  INNER JOIN Reiting ON Reiting.Kod_student = dbo_student.Kod_stud
GROUP BY dbo_groups.Kod_group;

SELECT
  Predmet.Nazva,
  AVG(Reiting.Reiting) AS AvgReit
FROM
  Predmet
  INNER JOIN Predmet_plan ON Predmet_plan.K_predmet = Predmet.K_predmet
  INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
  INNER JOIN Reiting ON Reiting.K_zapis = Rozklad_pids.K_zapis
GROUP BY Predmet.Nazva;

SELECT
  dbo_student.Sname,
  Predmet.Nazva,
  Reiting.Reiting
FROM
  Predmet
  INNER JOIN Predmet_plan ON Predmet_plan.K_predmet = Predmet.K_predmet
  INNER JOIN Rozklad_pids
    ON Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
  INNER JOIN Reiting
    ON Reiting.K_zapis = Rozklad_pids.K_zapis
  INNER JOIN dbo_student
    ON dbo_student.Kod_group = Rozklad_pids.Kod_group;
# GROUP BY dbo_student.Sname;

SELECT
  Predmet.Nazva,
  dbo_student.Name,
  MIN(Reiting.Reiting) AS MinReit
FROM Predmet
  INNER JOIN Predmet_plan ON Predmet_plan.K_predmet = Predmet.K_predmet
  INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
  INNER JOIN Reiting ON Reiting.K_zapis = Rozklad_pids.K_zapis
  INNER JOIN dbo_student ON dbo_student.Kod_group = Rozklad_pids.Kod_group
GROUP BY Predmet.Nazva, dbo_student.Name;

SELECT
  Predmet.Nazva,
  MAX(Reiting.Reiting) AS MaxReit
FROM (((Predmet
  INNER JOIN Predmet_plan ON Predmet_plan.K_predmet = Predmet.K_predmet)
  INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl)
  INNER JOIN Reiting ON Reiting.K_zapis = Rozklad_pids.K_zapis)
  INNER JOIN dbo_student ON dbo_student.Kod_group = Rozklad_pids.Kod_group
GROUP BY Predmet.Nazva;

SELECT
  SUM(Predmet_plan.Chas_Lek) lek,
  SUM(Predmet_plan.Cahs_pr)  prakt,
  SUM(Predmet_plan.Chas_all) `all`,
  Predmet.Nazva
FROM Predmet_plan
  INNER JOIN Predmet ON Predmet_plan.K_predmet = Predmet.K_predmet
GROUP BY Predmet.Nazva
ORDER BY `all` DESC;

SELECT DISTINCT
  q.Nazva,
  Count(q.Kod_group)
FROM (SELECT DISTINCT
        Spetsialnost.Nazva,
        Rozklad_pids.Kod_group
      FROM ((Spetsialnost
        INNER JOIN Navch_plan ON Navch_plan.K_spets = Spetsialnost.K_spets)
        INNER JOIN Predmet_plan ON Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan)
        INNER JOIN Rozklad_pids ON Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl) AS q
GROUP BY q.Nazva;